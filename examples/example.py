# -*- coding: utf-8 -*-

import logging

from sehnsucht import scheduler, add_scheduler_task

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


@scheduler(cron='* * * * * 1,10,30,40', retry_times=5, retry_interval=3)
def task1():
    "每分钟的第1秒，10秒，30秒，40秒执行一次"
    print("task 1")


@scheduler(cron='* * * * 15-40 5/10', retry_times=3, retry_interval=10)
def task2():
    "每小时的第15至40分钟内，从第5秒开始，每隔10秒执行一次"
    print("task 2")


@scheduler(cron='* * * * * *', retry_times=5, retry_interval=5)
def task3():
    "每秒执行一次"
    print("task 3")


@scheduler(cron='* * * * * 00', retry_times=3, retry_interval=5, excludes=(KeyError,), includes=(StopIteration,))
def task4():
    "每分钟执行一次"
    raise StopIteration


def task5():
    print("task 5")


add_scheduler_task(func=task5, cron='* * * * * 10')


def task6():
    print("task 6")


scheduler(cron="* * * * * 30")(task6)
